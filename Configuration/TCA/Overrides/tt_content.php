<?php
defined('TYPO3') or die();

call_user_func(static function () {

    //Adding Custom CType Item Group
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItemGroup(
        'tt_content',
        'CType',
        'teufels',
        'teufels',
        'after:special'
    );

    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['tt3modal_tt3_modal'] = 'tt3modal_plugin_icon';
    $tempColumns = [
        'backendtitle' => [
            'config' => [
                'type' => 'input',
            ],
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.backendtitle',
        ],
        'tx_tt3modal_openafter_active' => [
            'exclude' => 1,
            'label' => 'use "Open After" time/scrolling',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
            ],
            'onChange' => 'reload',
        ],
        'tx_tt3modal_openafter' => [
            'displayCond' => 'FIELD:tx_tt3modal_openafter_active:REQ:true',
            'config' => [
                'default' => '0',
                'eval' => 'int',
                'range' => [
                    'lower' => '0',
                ],
                'type' => 'input',
            ],
            'l10n_mode' => 'exclude',
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3modal_openafter',
            'description' => 'automatically open after x seconds 
            0 = do not open automatically',
        ],
        'tx_tt3modal_openscrolling' => [
            'displayCond' => 'FIELD:tx_tt3modal_openafter_active:REQ:true',
            'config' => [
                'default' => '0',
                'eval' => 'int',
                'range' => [
                    'lower' => '0',
                ],
                'type' => 'input',
            ],
            'l10n_mode' => 'exclude',
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3modal_openscrolling',
            'description' => 'open when scrolling after x px
            0 = do not open on scroll',
        ],
        'tx_tt3modal_closeafter_active' => [
            'exclude' => 1,
            'label' => 'use "Close After" time/scrolling',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
            ],
            'onChange' => 'reload',
        ],
        'tx_tt3modal_closeafter' => [
            'displayCond' => 'FIELD:tx_tt3modal_closeafter_active:REQ:true',
            'config' => [
                'default' => '0',
                'eval' => 'int',
                'range' => [
                    'lower' => '0',
                ],
                'type' => 'input',
            ],
            'l10n_mode' => 'exclude',
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3modal_closeafter',
            'description' => 'automatically close after x seconds 
            0 = do not close automatically',
        ],
        'tx_tt3modal_closescrolling' => [
            'displayCond' => 'FIELD:tx_tt3modal_closeafter_active:REQ:true',
            'config' => [
                'default' => '0',
                'eval' => 'int',
                'range' => [
                    'lower' => '0',
                ],
                'type' => 'input',
            ],
            'l10n_mode' => 'exclude',
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3modal_closescrolling',
            'description' => 'close when scrolling after x px
            0 = do not close on scroll',
        ],
        'tx_tt3modal_closebackdrop' => [
            'config' => [
                'items' => [],
                'renderType' => 'checkboxToggle',
                'type' => 'check',
                'default' => 1,
            ],
            'l10n_mode' => 'exclude',
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3modal_closebackdrop',
            'description' => 'close when click in backdrop',
        ],
        'tx_tt3modal_width' => [
            'config' => [
                'default' => '0',
                'eval' => 'int',
                'range' => [
                    'lower' => '0',
                ],
                'type' => 'input',
            ],
            'l10n_mode' => 'exclude',
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3modal_width',
            'description' => 'width of the modal in px
            0 = auto',
        ],
        'tx_tt3modal_content' => [
            'config' => [
                'appearance' => [
                    'collapseAll' => 1,
                    'enabledControls' => [
                        'delete' => 1,
                        'dragdrop' => 1,
                        'hide' => 1,
                        'info' => 1,
                        'localize' => 1,
                        'new' => 1,
                        'sort' => 1,
                    ],
                    'levelLinksPosition' => 'top',
                    'showAllLocalizationLink' => 1,
                    'showNewRecordLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'useSortable' => 1,
                ],
                'foreign_sortby' => 'sorting',
                'foreign_table' => 'tt_content',
                'overrideChildTca' => [
                    'columns' => [
                        'colPos' => [
                            'config' => [
                                'default' => 999,
                            ],
                        ],
                    ],
                ],
                'type' => 'inline',
                'foreign_field' => 'tx_tt3modal_content_parent',
            ],
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3modal_content',
            'description' => 'add custom content element(s) to modal (optional)',
        ],
        'tx_tt3modal_link' => [
            'config' => [
                'renderType' => 'inputLink',
                'type' => 'input',
            ],
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3modal_link',
            'description' => 'put a link around the whole modal (optional)',
        ],
        'tx_tt3modal_media' => [
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'sys_file_reference',
                'foreign_field' => 'uid_foreign',
                'foreign_sortby' => 'sorting_foreign',
                'foreign_table_field' => 'tablenames',
                'foreign_match_fields' => [
                    'fieldname' => 'tx_tt3modal_media',
                ],
                'foreign_label' => 'uid_local',
                'foreign_selector' => 'uid_local',
                'overrideChildTca' => [
                    'columns' => [
                        'uid_local' => [
                            'config' => [
                                'appearance' => [
                                    'elementBrowserType' => 'file',
                                    'elementBrowserAllowed' => 'gif,jpg,jpeg,bmp,png,pdf,svg,ai,mp3,wav,mp4,ogg,flac,opus,webm,youtube,vimeo',
                                ],
                            ],
                        ],
                    ],
                    'types' => [
                        [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;audioOverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;videoOverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                    ],
                ],
                'filter' => [
                    [
                        'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                        'parameters' => [
                            'allowedFileExtensions' => 'gif,jpg,jpeg,bmp,png,pdf,svg,ai,mp3,wav,mp4,ogg,flac,opus,webm,youtube,vimeo',
                            'disallowedFileExtensions' => '',
                        ],
                    ],
                ],
                'appearance' => [
                    'useSortable' => 1,
                    'headerThumbnail' => [
                        'field' => 'uid_local',
                        'height' => '45m',
                    ],
                    'enabledControls' => [
                        'info' => 1,
                        'new' => false,
                        'dragdrop' => 1,
                        'sort' => 0,
                        'hide' => 1,
                        'delete' => 1,
                        'localize' => 1,
                    ],
                    'elementBrowserEnabled' => 1,
                    'fileByUrlAllowed' => 1,
                    'fileUploadAllowed' => 1,
                ],
                'maxitems' => '1',
            ],
            'exclude' => 1,
            'label' => 'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3modal_media',
            'description' => 'add media (image/video) to modal',
        ],
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
    /*
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.CType.div._tt3modal_',
        '--div--',
    ];
    */
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        'LLL:EXT:tt3_modal/Resources/Private/Language/locallang_db.xlf:tt_content.CType.tt3modal_tt3_modal',
        'tt3modal_tt3_modal',
        // Icon
        'tt3modal_plugin_icon',
        // The group ID, if not given, falls back to "none" or the last used --div-- in the item array
        'teufels'
    ];

    // Create palette
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', '--palette--;;modal_opening');
    $GLOBALS['TCA']['tt_content']['palettes']['modal_opening']['showitem'] = 'tx_tt3modal_openafter_active, --linebreak--, tx_tt3modal_openafter, tx_tt3modal_openscrolling';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', '--palette--;;modal_closing');
    $GLOBALS['TCA']['tt_content']['palettes']['modal_closing']['showitem'] = 'tx_tt3modal_closeafter_active, --linebreak--, tx_tt3modal_closeafter, tx_tt3modal_closescrolling, --linebreak--,tx_tt3modal_closebackdrop';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', '--palette--;;modal_general');
    $GLOBALS['TCA']['tt_content']['palettes']['modal_general']['showitem'] = 'tx_tt3modal_width, --linebreak--, tx_tt3modal_link, --linebreak--, tx_tt3modal_media';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', '--palette--;;modal_ce');
    $GLOBALS['TCA']['tt_content']['palettes']['modal_ce']['showitem'] = 'tx_tt3modal_content';

    $tempTypes = [
        'tt3modal_tt3_modal' => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general, backendtitle, --div--;Modal,--palette--;Opening;modal_opening, --palette--;Closing;modal_closing,--palette--;General;modal_general,--palette--;Content Elements;modal_ce, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames, --palette--;LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.palette.backgroundimage4ce;backgroundimage4ce,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, --div--;Extended, tx_hiveovrttcontent_anchortargetname, tx_hiveovrttcontent_customcssclass',
        ],
    ];

    $GLOBALS['TCA']['tt_content']['types'] += $tempTypes;

    /**
     * Preview Renderer (needed for TYPO3 v12 backwards compatibility)
     */
    $GLOBALS['TCA']['tt_content']['types']['tt3modal_tt3_modal']['previewRenderer'] = \Teufels\Tt3Modal\Preview\PreviewRenderer::class;
});