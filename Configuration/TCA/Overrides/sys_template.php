<?php
defined('TYPO3') or die();

$extensionKey = 'tt3_modal';
$extensionTitle = '[teufels] Modal';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionKey, 'Configuration/TypoScript', $extensionTitle);