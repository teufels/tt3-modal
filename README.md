[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--modal-orange.svg)](https://bitbucket.org/teufels/tt3-modal/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3__modal-red.svg)](https://bitbucket.org/teufels/tt3-modal/src/main/)
![version](https://img.shields.io/badge/version-1.1.*-yellow.svg?style=flat-square)

[ ṯeufels ] Modal
==========
Modal / SlideOut / PopUp Element

#### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/13_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req teufels/tt3-modal`

***

### Requirements
- `Bootstrap v5` *based on Bootstrap modal https://getbootstrap.com/docs/5.0/components/modal/
***

### How to use
#### Administrator
- Install with composer
- Import Static Template (before sitepackage)
- Styling / Position / Animation via CSS & Template Override in sitepackage

####  Integrator
- place [teufels] Modal (CE) where modal should appear
- could used with Media, Appearance>Background, Content elements

***

### Update & Migration from hive_modal
1. in composer.json replace `beewilly/hive_modal` with `"teufels/tt3-modal":"^1.0"`
2. Composer update
3. Include TypoScript set `[teufels] Modal`
4. Analyze Database Structure -> Add tables & fields (do not remove old hive_modal yet)
5. Perform Upgrade Wizards `[teufels] Modal`
6. Analyze Database Structure -> Remove tables & unused fields (remove old hive_modal now)
7. class & id changed -> adjust styling in sitepackage (e.g. tx-hive-modal => tx-tt3-modal)
8. check & adjust be user group access rights
***

### Changelog
#### 1.1.x: support TYPO3 v13
- improve BE Preview
- custom preview renderer only used for TYPO3 v12, TYPO3 v13 uses default renderer
- add Previews for TYPO3 v13 and TYPO3 v12 support
#### 1.0.x: intial
- intial from [hive_modal](https://bitbucket.org/teufels/hive_modal/src/)