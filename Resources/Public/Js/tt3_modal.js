$(document).ready(function () {

    $( ".tx-tt3-modal" ).each(function( index ) {
        var $uid = $(this).data("uid");

        //if open-after-active ist set
        if($("#tt3_modal_" + $uid).data("open-after-active") > 0) {
            //if open-after ist set
            if($("#tt3_modal_" + $uid).data("open-after") > 0) {
                setTimeout(function(){
                    showModal($uid);
                }, ($("#tt3_modal_" + $uid).data("open-after")*1000));
            }
            //if open-scrolling ist set
            if($("#tt3_modal_" + $uid).data("open-scrolling") > 0) {
                window.addEventListener("scroll", function() {
                    if (window.scrollY > $("#tt3_modal_" + $uid).data("open-scrolling")) {
                        showModal($uid);
                    }
                });
            }
        } else {
           showModal($uid);
        }

        //if close-after ist set
        if($("#tt3_modal_" + $uid).data("close-after") > 0) {
            setTimeout(function(){
                $("#tt3_modal_" + $uid).modal("hide");
            }, ($("#tt3_modal_" + $uid).data("close-after")*1000));
        }

        //if close-scrolling ist set
        if($("#tt3_modal_" + $uid).data("close-scrolling") > 0) {
            window.addEventListener("scroll", function() {
                if (window.scrollY > $("#tt3_modal_" + $uid).data("close-scrolling")) {
                    $("#tt3_modal_" + $uid).modal("hide");
                }
            });
        }
    });

});

function showModal($uid) {
    //show if not already show
    if (document.cookie.indexOf("tt3_modal_" + $uid  + "=true") < 0) {
        $("#tt3_modal_" + $uid).modal("show");
        setCookie("tt3_modal_" + $uid,"true",$("#tt3_modal_" + $uid).data("cookie-lifetime"));
    }
}

function setCookie(cname, cvalue, exhours) {
    const d = new Date();
    d.setTime(d.getTime() + (exhours*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}