<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_modal" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Modal\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3modalPluginUpdater')]
class PluginUpdater implements UpgradeWizardInterface
{
    private const SOURCE_CTYPE = 'hivemodal_modal';
    private const TARGET_CTYPE = 'tt3modal_tt3_modal';

    public function getTitle(): string
    {
        return '[teufels] Modal: Migrate plugin';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard migrates all existing plugin to new one. Count of plugins: ' . count($this->getMigrationRecords());
        return $description;
    }

    public function getPrerequisites(): array
    {
        return [];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return count($this->getMigrationRecords()) > 0;
    }

    public function performMigration(): bool
    {
        $records = $this->getMigrationRecords();

        foreach ($records as $record) {
            $this->updateContentElement($record['uid'], self::TARGET_CTYPE);
        }

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tt_content');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid', 'CType')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->eq(
                    'CType',
                    $queryBuilder->createNamedParameter(self::SOURCE_CTYPE)
                )
            )
            ->executeQuery()
            ->fetchAllAssociative();
    }


    /**
     * Updates Ctype and plugindata of the given content element UID
     *
     * @param int $uid
     * @param string $newCtype
     */
    protected function updateContentElement(int $uid, string $newCtype): void
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tt_content');
        $queryBuilder->update('tt_content')
            ->set('CType', $newCtype)
            ->set('tx_tt3modal_closeafter_active',$queryBuilder->quoteIdentifier('tx_hivemodal_closeafter_active'),false)
            ->set('tx_tt3modal_closeafter',$queryBuilder->quoteIdentifier('tx_hivemodal_closeafter'),false)
            ->set('tx_tt3modal_closescrolling',$queryBuilder->quoteIdentifier('tx_hivemodal_closescrolling'),false)
            ->set('tx_tt3modal_closebackdrop',$queryBuilder->quoteIdentifier('tx_hivemodal_closebackdrop'),false)
            ->set('tx_tt3modal_openafter_active',$queryBuilder->quoteIdentifier('tx_hivemodal_openafter_active'),false)
            ->set('tx_tt3modal_openafter',$queryBuilder->quoteIdentifier('tx_hivemodal_openafter'),false)
            ->set('tx_tt3modal_openscrolling',$queryBuilder->quoteIdentifier('tx_hivemodal_openscrolling'),false)
            ->set('tx_tt3modal_width',$queryBuilder->quoteIdentifier('tx_hivemodal_width'),false)
            ->set('tx_tt3modal_link',$queryBuilder->quoteIdentifier('tx_hivemodal_link'),false)
            ->set('tx_tt3modal_media',$queryBuilder->quoteIdentifier('tx_hivemodal_media'),false)
            ->set('tx_tt3modal_content',$queryBuilder->quoteIdentifier('tx_hivemodal_content'),false)
            ->set('tx_tt3modal_content_parent',$queryBuilder->quoteIdentifier('tx_hivemodal_content_parent'),false)

            ->where(
                $queryBuilder->expr()->in(
                    'uid',
                    $queryBuilder->createNamedParameter($uid, Connection::PARAM_INT)
                )
            )
            ->executeStatement();
    }

}
